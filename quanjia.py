#!/usr/bin/python
# -*- coding: UTF-8 -*-

## 全家签到
# 登录H5：https://fmapp-activity.chinafamilymart.com.cn/login
# https://fmapp-activity.chinafamilymart.com.cn/pages/sudokuDrawLottery/index?query=%257B%2522activitycode%2522%253A%25223420210128001%2522%252C%2522activitytype%2522%253A%252234%2522%252C%2522memberId%2522%253A%25225974385974734d6277324a7855587937432f414c48673d3d%2522%252C%2522longitude%2522%253A%2522121.42084625683843%2522%252C%2522latitude%2522%253A%252231.19731768067037%2522%252C%2522city%2522%253A%2522%25E5%25B9%25BF%25E5%25B7%259E%2522%252C%2522citycode%2522%253A%2522004%2522%252C%2522appVersion%2522%253A%25222.1.1%2522%257D
import requests
import sys
import os
from urllib import parse
import json

env = os.getenv("ENV")

if env == None or len(env) == 0:
    print("环境名为空")
    sys.exit()
elif "QUANJIA" != env:
    print("非全家脚本")
    sys.exit()

token = os.getenv("TOKEN")
#用户id
memberId = os.getenv("MEMBER_ID")
api = "https://fmapp.chinafamilymart.com.cn/api/app/market/member/signin/sign"
headers = {
    "os":"ios",
    "deviceId":"58F77AA4-E879-46CF-8A19-4A287F188C56",
    "fmVersion":"2.1.1",
    "loginChannel": "app",
    "token": token
}

def qiandao():
    r = requests.post(api, data={}, headers=headers)
    print("1. 签到")
    print(r.text)

zhuanpan_api = "https://fmapp-activity.chinafamilymart.com.cn/pages/sudokuDrawLottery/index?query="
#定位
dingwen={
	"activitycode": "",
	"activitytype": "34",
	"memberId": memberId,
	"longitude": "121.42084625683843",
	"latitude": "31.19731768067037",
	"city": "广州",
	"citycode": "004",
	"appVersion": "2.1.1"
}
#body
huodong={
  "activityCode": ""
}

def urlen():
    return parse.quote(parse.quote(json.dumps(dingwen, ensure_ascii=False)))

def zhuanpan():
    hheaders = {
        "token": token,
        "Referer": zhuanpan_api + urlen(),
        "Content-Type":"application/json"
    }
    r = requests.post("https://fmapp.chinafamilymart.com.cn/api/app/market/turn/lottery", data=json.dumps(huodong), headers= hheaders)
    print(r.text)

def zhuanpan_beijing():
    dingwen["longitude"] = "121.36965313162946"
    dingwen["latitude"] = "31.172685775335516"
    dingwen["city"] = "北京"
    dingwen["citycode"] = "010"
    dingwen["activitycode"]="3420210301001"
    huodong["activityCode"]="3420210301001"
    zhuanpan()
    return
def zhuanpan_shanghai():
    dingwen["longitude"] = "121.36967906714295"
    dingwen["latitude"] = "31.17267426419658"
    dingwen["city"] = "上海"
    dingwen["citycode"] = "001"
    dingwen["activitycode"]="3420210226002"
    huodong["activityCode"]="3420210226002"
    zhuanpan()
    return
def zhuanpan_guangzhou():
    dingwen["longitude"] = "121.36966827764408"
    dingwen["latitude"] = "31.172661692138757"
    dingwen["city"] = "广州"
    dingwen["citycode"] = "004"
    dingwen["activitycode"]="3420210226001"
    huodong["activityCode"]="3420210226001"
    zhuanpan()
    return
def zhuanpan_chengdu():
    dingwen["longitude"] = "121.3696245041284"
    dingwen["latitude"] = "31.17268257983394"
    dingwen["city"] = "成都"
    dingwen["citycode"] = "006"
    dingwen["activitycode"]="3420210226001"
    huodong["activityCode"]="3420210226001"
    zhuanpan()
    return

qiandao()

#北京-转盘
print("北京转盘")
zhuanpan_beijing()
# #上海-转盘
print("上海转盘")
zhuanpan_shanghai()
# #广州-转盘
print("广州转盘")
zhuanpan_guangzhou()
zhuanpan_guangzhou()
# #成都-转盘
print("成都转盘")
zhuanpan_chengdu()



